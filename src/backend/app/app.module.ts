import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { StopsController } from "./stops.controller";

@Module({
  controllers: [AppController, StopsController]
})
export class AppModule {}