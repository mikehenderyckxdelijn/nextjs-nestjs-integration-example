import { useState, useEffect } from "react";
import Link from "next/link";

const Stops = () => {
  return (
    <>
      <h1>Haltes</h1>
      <ul>
        <li>
          <Link href="/stops/103377">
            <a>Halte 103377</a>
          </Link>
        </li>
        <li>
          <Link href="/stops/101200">
            <a>Halte 101200</a>
          </Link>
        </li>
        <li>
          <Link href="/stops/103271">
            <a>Halte 103377</a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Stops;
