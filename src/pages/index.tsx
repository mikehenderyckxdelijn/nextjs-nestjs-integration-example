import { useState, useEffect } from "react";
import Link from "next/link";

function useRandomNumber() {
  const [number, setNumber] = useState<number>();

  useEffect(() => {
    fetch("/api/randomNumber")
      .then((response) => response.text())
      .then((text) => setNumber(+text));
  }, []);

  return number;
}

const Home = () => {
  const number = useRandomNumber();
  return (
    <>
      <p>Random Number: {number}</p>
      <ul>
        <li>
          <Link href="/stops">
            <a>Haltes</a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Home;
