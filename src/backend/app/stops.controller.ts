import { Controller, Get, Param } from "@nestjs/common";

@Controller("stops")
export class StopsController {
  @Get()
  stops() {
    return [1, 2, 3];
  }

  @Get("/:stopId")
  async findOne(@Param("stopId") param: string) {
    console.log("Must fetch", param);
    const stop = await(
      await fetch(
        `https://accept.delijn.be/rise-api-core/haltes/laatstbekekenhaltes/${param}`
      )
    ).json();
    return stop;
  }
}
