import * as React from "react";
import { useRouter } from "next/router";

// export async function getStaticPaths() {
//   return {
//     paths: [{ params: { id: "103377" } }, { params: { id: "101200" } }],
//     fallback: true,
//   };
// }

// export async function getStaticProps({ params }) {
//   const stop = await (
//     await fetch(
//       // `https://accept.delijn.be/rise-api-core/haltes/laatstbekekenhaltes/${params.id}`
//       // `/api/stops/${params.id}`
//       `http://localhost:3000/api/stops/${params.id}`
//     )
//   ).json();

//   return {
//     props: { stop },
//     revalidate: 10,
//   };
// }

export async function getServerSideProps(context) {
  const {
    // req,
    // res,
    // query,
    // resolvedUrl,
    // locales,
    // locale,
    // defaultLocale,
    params,
  } = context;

  const res = await fetch(
    `https://accept.delijn.be/rise-api-core/haltes/laatstbekekenhaltes/${params.id}`
  );
  const stop = await res.json();

  return {
    props: { stop },
  };
}

export const Stops = ({ stop }): JSX.Element => {
  const router = useRouter();

  return (
    <div>
      <h1>Stop {router.query.id}</h1>
      <pre>{JSON.stringify(stop, null, 2)}</pre>
    </div>
  );
};

export default Stops;
