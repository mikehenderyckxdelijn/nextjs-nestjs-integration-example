import { Backend } from "../../backend/main";
import { NextApiRequest, NextApiResponse } from "next";

console.log('Stitching catchAll route')
export default (req: NextApiRequest, res: NextApiResponse) => new Promise(async resolve => {
  console.log('is this happeninge?')
  const listener = await Backend.getListener();
  listener(req, res);
  res.on("finish", resolve);
})